# Gitlab Test

![](https://img.shields.io/npm/v/@loicpetit/gitlabtest.svg)
![](https://img.shields.io/bundlephobia/min/@loicpetit/gitlabtest.svg)
[NPM Package](https://www.npmjs.com/package/@loicpetit/gitlabtest)

Test how create a module in NodeJS and host code in Gitlab.

## Install

```
npm install @loicpetit/gitlabtest
```

## Usage

```
const test = require('@loicpetit/gitlabtest')

const hello = test.hello('Loïc')
console.log(hello)
// expect 'Hello Loïc !'
```
