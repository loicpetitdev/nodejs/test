const fs = require('fs')

exports.hello = function(name){
    if(name) {
        return `Hello ${name} !`
    }
    else {
        return 'Hello !'
    }
}

/**
 * Get a file
 * @param path : must be an absolute path
 */
exports.getFile = function(path){
    return new Promise((resolve, reject) => {
        if(path && fs.existsSync(path)){
            fs.readFile(path, 'utf-8', (err, content) => {
                if(!err){
                    resolve(content)
                }
                else {
                    reject(err)
                }
            })
        }
        else {
            reject('file not found')
        }
    })
}
