const expect = require('chai').expect
const rewiremock = require('rewiremock').default
const path = require('path')

describe('Index', () => {

    describe('Hello', () => {
    
        it('should say hello with a parameter', () => {
            const index = require('./index')
            const name = "Rox"
            const expected = "Hello Rox !"
            const result = index.hello(name)
            expect(result).to.equals(expected)
        })
    
        it('should say hello without parameter', () => {
            const index = require('./index')
            const expected = "Hello !"
            const result = index.hello()
            expect(result).to.equals(expected)
        })
    
    })
    
    describe('Get File', () => {

        describe('Mock', () => {
    
            //  Constants
            const existingPath = '../test/file.txt'
            const notExistingPath = './test.notexist.txt'
            const content = 'My file content !'
            
            //  Mock
            function existsSync(path){
                return path && existingPath && path == existingPath
            }

            function readFile(path, options, callback){
                callback(null, content)
            }
            rewiremock('fs').with({
                existsSync: existsSync,
                readFile: readFile
            })
            rewiremock.passBy('./index')

            beforeEach( () => {      
                rewiremock.isolation() 
                rewiremock.enable()
            })
    
            afterEach(() => {
                rewiremock.disable()
                rewiremock.withoutIsolation()
            })
            
            it('should get file', () => {
                const index = require('./index')
                return index.getFile(existingPath).then((result) => {
                    expect(result).to.equals(content)
                }).catch((err) => {
                    throw err
                })
            })
            
            it('should reject if file not found', () => {
                const index = require('./index')
                return index.getFile(notExistingPath).then((result) => {
                    throw 'should not get file'
                }).catch((err) => {
                    expect(err).to.equals('file not found')
                })
            })
            
            it('should reject if no path', () => {
                const index = require('./index')
                return index.getFile().then((result) => {
                    throw 'should not get file'
                }).catch((err) => {
                    expect(err).to.equals('file not found')
                })
            })

        })
    
        describe('Integration', () => {
            
            const filePath = path.join(__dirname, '../test/index.txt')

            it('should get file', () => {
                const index = require('./index')
                return index.getFile(filePath).then((result) => {
                    expect(result).to.equals('True content')
                }).catch((err) => {
                    throw err
                })
            })

        })
    })

})
